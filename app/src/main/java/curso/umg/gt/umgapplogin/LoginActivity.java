package curso.umg.gt.umgapplogin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    private EditText et1, et2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        et1 = (EditText) findViewById(R.id.et1);
        et2 = (EditText) findViewById(R.id.et2);
    }

    public void login(View view) {

        RadioButton r1=(RadioButton)findViewById(R.id.radio_teachers);
        RadioButton r2=(RadioButton)findViewById(R.id.radio_pupil);

        String user = et1.getText().toString();
        String pass = et2.getText().toString();

        if (r1.isChecked()== true)
            if (user.endsWith("@umg.edu.gt") && pass.equals("catedratico123")) {
                Intent i = new Intent(this, TeacherActivity.class);
                startActivity(i);
                Toast notification = Toast.makeText(this, "Catedratico aceptado", Toast.LENGTH_SHORT);
                notification.show();
            }else {
                Toast notification = Toast.makeText(this, "Credenciales invalidas ", Toast.LENGTH_SHORT);
                notification.show();
            }
        if (r2.isChecked()==true)
            if (user.endsWith("@miumg.edu.gt") && pass.equals("alumno123")) {
                Intent i = new Intent(this, PupilActivity.class);
                startActivity(i);
                Toast notification = Toast.makeText(this, "Alumno aceptado ", Toast.LENGTH_SHORT);
                notification.show();
            }else {
                Toast notification = Toast.makeText(this, "Credenciales invalidas ", Toast.LENGTH_SHORT);
                notification.show();
            }
    }

    public void help (View view){
        Intent inHelp = new Intent(this, HelpActivity.class);
        startActivity(inHelp);
    }

    public void exit (View view) {
        Toast notification = Toast.makeText(this, "Salir ", Toast.LENGTH_SHORT);
        notification.show();
            finish();
    }
}
package curso.umg.gt.umgapplogin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import static android.widget.Toast.*;
import static android.widget.Toast.LENGTH_LONG;

public class PupilActivity extends AppCompatActivity {

    Spinner opciones;

    public void Test1 (View view){
        final RadioButton rbP, rbH, rbB;
        final TextView tvRespuesta;

        //Metodo Radio Button primer test
        rbB = (RadioButton) findViewById(R.id.rbBecerro);
        rbP = (RadioButton) findViewById(R.id.rbPerro);
        rbH = (RadioButton) findViewById(R.id.rbHiena);
        tvRespuesta = (TextView) findViewById(R.id.tvRespuesta);

        if (rbB.isChecked()== true){
            tvRespuesta.setText("Incorrecto");
        }else {
            tvRespuesta.setText("Incorrecto");
        }
        if (rbP.isChecked()==true){
            tvRespuesta.setText("Incorrecto");
        }else {
            tvRespuesta.setText("Incorrecto");
        }
        if (rbH.isChecked()==true){
            tvRespuesta.setText("Correcto");
        }else {
            tvRespuesta.setText("Incorrecto ");
        }

     }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pupil);

        // Metodo Spinner segundo Test

        opciones = (Spinner) findViewById(R.id.SpinnerSol);
        ArrayAdapter <CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.opcSol, android.R.layout.simple_spinner_item);
        opciones.setAdapter(adapter);

        opciones.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i){
                    case 1:
                        Toast toa1 = makeText(getApplicationContext(), "Respuesta Inconrrecta", LENGTH_LONG);
                        toa1.show();
                        break;
                    case 2:
                        Toast toa2 = makeText(getApplicationContext(), "Respuesta Inconrrecta", LENGTH_LONG);
                        toa2.show();
                        break;
                    case 3:
                        Toast toa3 = makeText(getApplicationContext(), "Respuesta Correcta", LENGTH_LONG);
                        toa3.show();
                        break;
                    case 4:
                        Toast toa4 = makeText(getApplicationContext(), "Respuesta Inconrrecta", LENGTH_LONG);
                        toa4.show();
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    // Metodo para calificacion con Estrellas
    public void RTBar (View view){
        RatingBar rtBar = (RatingBar) findViewById(R.id.ratingBar);;
        float ratingStars = rtBar.getRating();
        int numStars = rtBar.getNumStars();

        Toast rbToast1, rbToast2;

        rbToast2 = Toast.makeText(this, "Cantidad de Estrellas: "+ numStars, Toast.LENGTH_LONG);
        rbToast2.show();

        rbToast1 = Toast.makeText(this, "Puntuacion "+ ratingStars, Toast.LENGTH_LONG);
        rbToast1.show();
        }

    // Metodo para regresar a la pagina principal

    public void regresarLogin (View view){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    public void tgButton (View view){
        ToggleButton toggleButton = (ToggleButton) findViewById(R.id.toggleSiNo);

        if (toggleButton.isChecked()){
            Toast tgBtn1 = Toast.makeText(this, "Correcto", Toast.LENGTH_LONG);
            tgBtn1.show();
        }else {
            Toast tgBtn2 = Toast.makeText(this, "Incorrecto", Toast.LENGTH_LONG);
            tgBtn2.show();
        }
    }
}
